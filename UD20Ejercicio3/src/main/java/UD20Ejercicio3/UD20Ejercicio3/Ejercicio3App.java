package UD20Ejercicio3.UD20Ejercicio3;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JButton;

public class Ejercicio3App extends JFrame {

	private JPanel contentPane;
	private JLabel numeroBoton1;
	private JLabel numeroBoton2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio3App frame = new Ejercicio3App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio3App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel lblNewLabel = new JLabel("Boton 1: ");
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Boton 2:");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNewLabel, 0, SpringLayout.NORTH, lblNewLabel_1);
		sl_contentPane.putConstraint(SpringLayout.EAST, lblNewLabel_1, -124, SpringLayout.EAST, contentPane);
		contentPane.add(lblNewLabel_1);
		
		JButton boton1 = new JButton("Boton 1");
		sl_contentPane.putConstraint(SpringLayout.NORTH, boton1, 107, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, boton1, 83, SpringLayout.WEST, contentPane);
		contentPane.add(boton1);
		
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNum(1);
			}
		});
		
		JButton boton2 = new JButton("Boton 2");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblNewLabel_1, -22, SpringLayout.NORTH, boton2);
		sl_contentPane.putConstraint(SpringLayout.NORTH, boton2, 0, SpringLayout.NORTH, boton1);
		sl_contentPane.putConstraint(SpringLayout.EAST, boton2, -95, SpringLayout.EAST, contentPane);
		contentPane.add(boton2);
		
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNum(2);
			}
		});
		
		numeroBoton1 = new JLabel("0");
		sl_contentPane.putConstraint(SpringLayout.WEST, numeroBoton1, 132, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, lblNewLabel, -6, SpringLayout.WEST, numeroBoton1);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, numeroBoton1, -22, SpringLayout.NORTH, boton1);
		contentPane.add(numeroBoton1);
		
		numeroBoton2 = new JLabel("0");
		sl_contentPane.putConstraint(SpringLayout.WEST, numeroBoton2, 6, SpringLayout.EAST, lblNewLabel_1);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, numeroBoton2, -22, SpringLayout.NORTH, boton2);
		contentPane.add(numeroBoton2);
	}
	
	public void addNum(int btn) {
		if (btn == 1) {
			numeroBoton1.setText(String.valueOf(Integer.parseInt(numeroBoton1.getText()) + 1));
		} else if (btn == 2) {
			numeroBoton2.setText(String.valueOf(Integer.parseInt(numeroBoton2.getText()) + 1));
		}
	}
}
